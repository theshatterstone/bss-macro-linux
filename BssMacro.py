import os
import cv2
import numpy as np
import time
import threading
from pynput.keyboard import Key, Listener
from matplotlib import pyplot as plt

n=0
on_off = True
isBagFull = False
isE = False
isSpawn = False
isHiveRock = False
directory = os.path.realpath(__file__)
dir2 = os.path.dirname(directory)
# First, take a screenshot to ensure tempimg.png exists
os.system("wayshot -f " + dir2 + "/tempimg.png") # wayshot required; wayland-only
os.system("wayshot -s '337,82 1164x303' -f " + dir2 + "/tempimg2.png") # wayshot AND slurp required; wayland-only
os.system("wayshot -s '465,44 991x118' -f " + dir2 + "/tempimg3.png") # wayshot AND slurp required; wayland-only
spawn_detect_path = dir2 + '/img/spawn-detect.png'
hiverock_detect_path = dir2 + '/img/correct-detect.png'
collectimg_path = dir2 + '/img/collectpollen.png'
# convertimg_path = dir2 + '/img/makehoney.png'
convertimg_path = dir2 + '/img/whiteline.png'
grassd_path = dir2 + '/img/grassD.png'
grassn_path = dir2 + '/img/grassN.png'

eb_path = dir2 + '/img/eb.png'
target_path = dir2 + '/tempimg.png'
small_target_path = dir2 + '/tempimg2.png'
top_target_path = dir2 + '/tempimg3.png'

print(eb_path)
print(target_path)

# A clean slate with only 1 instance of ydotoold running
os.system("killall ydotoold")
os.system("ydotoold &")

# Apply 5s wait time to let player switch to the game before the macro starts
time.sleep(5)

# Function to check if an image, template, is within a screenshot, target
def is_image_within_image(template_path, target_path):
    global n
    # Load images
    template = cv2.imread(template_path)
    # assert template is not None, "Template (image within img/) could not be read, check with os.path.exists()"
    target = cv2.imread(target_path)
    # assert target is not None, "Target (screenshot: tempimg.png) could not be read, check with os.path.exists()"

    try:
        template = cv2.imread(template_path)
        if template is None:
            raise ValueError("Template image could not be read.")
        
        target = cv2.imread(target_path)
        if target is None:
            raise ValueError("Target image could not be read.")
    except Exception as e:
        print(f"Error: {e}")
        return False

    # # Convert images to grayscale
    template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
    target_gray = cv2.cvtColor(target, cv2.COLOR_BGR2GRAY)

    # Match the template within the target image
    result = cv2.matchTemplate(target, template, cv2.TM_CCOEFF_NORMED)
    
    # w,h = template_gray.shape[::-1]
    # Define a threshold for matching
    threshold = 1
    
    # Find the location(s) where the template matches
    loc = np.where(result >= threshold)
    print(loc)
    # visualization = target.copy()  # Create a copy of the target image for visualization
    # for pt in zip(*loc[::-1]):
    #     print(pt)
    #     cv2.rectangle(visualization, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
    # cv2.imwrite('res'+str(n)+'.png', visualization)
    # Check if any matches are found
    detected = len(loc[0]) > 0
    
    return detected

# os.system("ydotool mousemove --absolute -x 500 -y 300")
def returnToHive():
        # Escape, R, Enter aka Reset Character (1=Esc, 19=R, 28=Enter/Return)
    global n
    os.system("ydotool key 1:1")
    time.sleep(0.1)
    os.system("ydotool key 1:0")
    os.system("ydotool key 19:1")
    time.sleep(0.1)
    os.system("ydotool key 19:0")
    os.system("ydotool key 28:1")
    time.sleep(0.1)
    os.system("ydotool key 28:0")

    # 8s waiting to respawn before moving
    time.sleep(8)

    # Press "o" 5 times to fully zoom out (24=Letter O)
    os.system("ydotool key 24:1")
    time.sleep(0.1)
    os.system("ydotool key 24:0")
    os.system("ydotool key 24:1")
    time.sleep(0.1)
    os.system("ydotool key 24:0")
    os.system("ydotool key 24:1")
    time.sleep(0.1)
    os.system("ydotool key 24:0")
    os.system("ydotool key 24:1")
    time.sleep(0.1)
    os.system("ydotool key 24:0")
    os.system("ydotool key 24:1")
    time.sleep(0.1)
    os.system("ydotool key 24:0")

    time.sleep(5)
    # # Check if at the right position (in front of hive, not behind it or at the spawnpoint)
    # # First, take a screenshot
    os.system("wayshot -f " + dir2 + "/tempimg.png") # wayshot required; wayland-only
    os.system("wayshot -s '337,82 1164x303' -f " + dir2 + "/tempimg2.png") # wayshot AND slurp required; wayland-only
    os.system("wayshot -s '465,44 991x118' -f " + dir2 + "/tempimg3.png") # wayshot AND slurp required; wayland-only
    # # Then detect if the needed items are present within the image
    # isE works but isConvert and isCollect dont work
    isE = is_image_within_image(eb_path, top_target_path)
    n+=1
    isConvert = is_image_within_image(convertimg_path, top_target_path)
    n+=1
    isCollect = is_image_within_image(collectimg_path, top_target_path)
    n+=1
    isGrassD = is_image_within_image(grassd_path, target_path)
    n+=1
    isGrassN = is_image_within_image(grassn_path, target_path)
    n+=1
    isSpawn = is_image_within_image(spawn_detect_path, target_path)
    n+=1
    isHive = (isGrassD or isGrassN) and not isSpawn
    # isHive = isE #isE and (isGrassD or isGrassN)
    if isHive == False:
        print("Not at correct hive position; Retrying...")
        print("isHive =", isHive, "isE=", isE, "isConvert=", isConvert, "isCollect=", isCollect, "isGrassD=", isGrassD, "isGrassN=", isGrassN)

        returnToHive()
        # return 1
    elif isHive == True:
        print("At correct Hive position")
        print("isHive =", isHive, "isE=", isE, "isConvert=", isConvert, "isCollect=", isCollect, "isGrassD=", isGrassD, "isGrassN=", isGrassN)
        # return 1
    
        if isE:
            os.system("ydotool key 18:1")
            time.sleep(0.1)
            os.system("ydotool key 18:0")
            while isE:
                os.system("wayshot -s '465,44 991x118' -f " + dir2 + "/tempimg3.png") # wayshot AND slurp required; wayland-only
                isE = is_image_within_image(eb_path, top_target_path)
            
    # quit()
    return 0

def mainMacro():
    # Hold down D for 7,500 ms or 7.5s (32=D)
    os.system("ydotool key 32:1")
    time.sleep(7.5)
    os.system("ydotool key 32:0")

    # Do a jump to the right to step on ramp (32=D, 57=Space)
    # os.system("ydotool key -d 300 32:1 57:1 57:0 32:0")
    os.system("ydotool key 32:1")
    time.sleep(0.3)
    os.system("ydotool key 57:1")
    time.sleep(0.3)
    os.system("ydotool key 57:0")
    # Shortening down this delay for faster release of D after Space is released
    # to ensure character doesnt move too far right, going into the cannon itself
    time.sleep(0.2)
    os.system("ydotool key 32:0")

    print("On Ramp")

    # Go to the wall of the ticket tent (17=W)
    # os.system("ydotool key -d 2000 17:1 17:0")
    os.system("ydotool key 17:1")
    time.sleep(2)
    os.system("ydotool key 17:0")

    # Go right at ticket tent wall
    # os.system("ydotool key -d 1000 32:1 32:0")
    os.system("ydotool key 32:1")
    time.sleep(1)
    os.system("ydotool key 32:0")

    # Get to Red Cannon from ticket tent
    # os.system("ydotool key -d 1000 31:1 31:0")
    os.system("ydotool key 31:1")
    time.sleep(1)
    os.system("ydotool key 31:0")

    # Use Image detection to check whether "E" on Red Cannon is visible on Screen
    # First, take a screenshot
    os.system("wayshot -s '465,44 991x118' -f " + dir2 + "/tempimg3.png") # wayshot AND slurp required; wayland-only
    # Then detect if the "E" is present within the image
    isE = is_image_within_image(eb_path, top_target_path)
    if isE == False:
        print("Failed to find E in image")
        returnToHive()
        return 1
    
    # Press E to use Red Cannon
    # os.system("ydotool key -d 100 18:1 18:0")
    os.system("ydotool key 18:1")
    time.sleep(0.1)
    os.system("ydotool key 18:0")
    time.sleep(5)

    # Move to Wall of Mountain Top Gate and Down into the Bamboo field
    # os.system("ydotool key -d 3000 17:1 17:0")
    os.system("ydotool key 17:1")
    time.sleep(3)
    os.system("ydotool key 17:0")
    # os.system("ydotool key -d 800 30:1 30:0")
    os.system("ydotool key 30:1")
    time.sleep(0.8)
    os.system("ydotool key 30:0")
    # os.system("ydotool key -d 500 17:1 17:0")
    os.system("ydotool key 17:1")
    time.sleep(0.5)
    os.system("ydotool key 17:0")
    # os.system("ydotool key -d 1500 30:1 30:0")
    os.system("ydotool key 30:1")
    time.sleep(1.5)
    os.system("ydotool key 30:0")
    # os.system("ydotool key -d 4000 17:1 17:0")
    os.system("ydotool key 17:1")
    time.sleep(4)
    os.system("ydotool key 17:0")

    # Move back from Panda Platform to Bamboo
    # os.system("ydotool key -d 1500 31:1 31:0")
    os.system("ydotool key 31:1")
    time.sleep(1.5)
    os.system("ydotool key 31:0")

    # Press the "1" key to Place Sprinkler
    # os.system("ydotool key -d 500 2:1 2:0")
    os.system("ydotool key 2:1")
    time.sleep(0.5)
    os.system("ydotool key 2:0")
    
    # Use << to rotate 180 degrees to turn towards the 10 bee gate ramp
    # and away from Spider field for better Tide Popper coverage
    os.system("ydotool type '<<'")

    # Shift for Shift-Lock
    # os.system("ydotool key -d 100 42:1 42:0")
    os.system("ydotool key 42:1")
    time.sleep(0.1)
    os.system("ydotool key 42:0")

    # Hold Down Left Click
    os.system("ydotool click 0x40")

    # Do pattern for 10 Minutes (60*10)
    t_end = time.time() + 60 * 10
    while time.time() < t_end:
        # Do a Square Pattern (S,D,W,A)
        # os.system("ydotool key -d 1000 31:1 31:0")
        os.system("ydotool key 31:1")
        time.sleep(0.8)
        os.system("ydotool key 31:0")
        # os.system("ydotool key -d 1000 32:1 32:0")
        os.system("ydotool key 32:1")
        time.sleep(0.8)
        os.system("ydotool key 32:0")
        # os.system("ydotool key -d 1000 17:1 17:0")
        os.system("ydotool key 17:1")
        time.sleep(0.8)
        os.system("ydotool key 17:0")
        # os.system("ydotool key -d 1000 30:1 30:0")
        os.system("ydotool key 30:1")
        time.sleep(0.8)
        os.system("ydotool key 30:0")

    # Release Left Click
    os.system("ydotool click 0x80")

    # Face the right way again and redisable Shift-Lock
    os.system("ydotool type '>>'")
    # os.system("ydotool key -d 100 42:1 42:0")
    os.system("ydotool key 42:1")
    time.sleep(0.1)
    os.system("ydotool key 42:0")

    # Return to Hive: Reset Character, Zoom Out, check for correct position
    returnToHive()
    # Wait for baloons to float back to the hive (Optional)
    # time.sleep(45)
    # Press "E" to convert Honey from Baloon (converts for 6 mins)
    os.system("ydotool key 18:1")
    time.sleep(0.5)
    os.system("ydotool key 18:0")
    t_end = time.time() + 60 * 6
    while time.time() < t_end:
        continue

    # print("Conversion Done")

# def check_press(key):
#     global on_off
#     if key == Key.q and on_off:
#         on_off = False
#         print("Macro Killed")
#         os.system("killall ydotoold")
#         quit()


# Macro Starts Running Here
returnToHive()
# Checks if you hit the kill button. If you did not hit the kill button, run the mainMacro() function. 
while True:
    while on_off == True:
      mainMacro()


# with Listener(
#         on_press=check_press,
#         on_release=on_release) as listener:
#     listener.join()


quit()
